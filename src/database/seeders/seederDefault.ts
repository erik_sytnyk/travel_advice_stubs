import * as dateFns from 'date-fns';
import * as bcrypt from 'bcrypt-nodejs';
import * as fs from 'fs-extra';

import path from '../../helpers/pathHelper';

export default {
  seedData,
};

async function seedData(db) {
  let seedPath = path.getDataRelative('seed/seedData.json');
  let seedData = await fs.readJson(seedPath);

  let userLookup = await seedUsers(db, seedData.users);
}

async function seedUsers(db, usersData) {
  let userLookup = {};

  let User = db.models.User;

  await User.remove();

  for (let user of usersData) {
    let localProfile = user.profile.local;

    localProfile.password = bcrypt.hashSync(localProfile.password, bcrypt.genSaltSync(8), null);

    let userModel = await User.create(user);

    userLookup[user.id] = userModel._id;
  }

  return userLookup;
}
