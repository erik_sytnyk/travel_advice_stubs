import * as Joi from 'joi';
import * as _ from 'lodash';
import * as fs from 'fs-extra';

import helper from './_controllerHelper';
import pathHelper from '../helpers/pathHelper';

import userRepository from '../repositories/userRepository';

export default {
  currentUser,
  usersList,
  travelAdviceSearch,
  getStatistics,
  getAdminData,
  getAccountDetails,
  getPlans,
  sendMessage
};

async function currentUser(req, res) {
  try {
    let userId = helper.getCurrentUser(req)._id;

    let user = await userRepository.getUserById(userId);

    const local = user.profile.local;

    let userMapped = {
      id: user.id,
      email: user.email,
      first_name: local.firstName,
      last_name: local.lastName,
      role: local.role
    };

    return helper.sendData(userMapped, res);
  } catch (err) {
    return helper.sendFailureMessage(err, res);
  }
}

async function usersList(req, res) {
  try {
    let usersPath = pathHelper.getDataRelative('stubs/users.json');
    let usersData = await fs.readJson(usersPath);

    return helper.sendData(usersData.data, res);
  } catch (err) {
    return helper.sendFailureMessage(err, res);
  }
}

async function travelAdviceSearch(req, res) {
  try {
    let data = await helper.loadSchema(req.params, {
      search: Joi.string().required(),
    });

    let searchPath = pathHelper.getDataRelative('stubs/search.json');

    let result = await fs.readJson(searchPath);

    return helper.sendData(result, res);
  } catch (err) {
    return helper.sendFailureMessage(err, res);
  }
}

async function getStatistics(req, res) {
  try {
    let data = await helper.loadSchema(req.params, {
      search: Joi.string().required(),
    });

    let covidStatsPath = pathHelper.getDataRelative('stubs/covidStats.json');
  
    let result = await fs.readJson(covidStatsPath);

    return helper.sendData(result, res);
  } catch (err) {
    return helper.sendFailureMessage(err, res);
  }
}

async function getAdminData(req, res) {
  let usersPath = pathHelper.getDataRelative('stubs/users.json');
  let usersData = await fs.readJson(usersPath);


  let customers  = usersData.data.map(user => {
    let month = _.random(1, 11);
    if (month < 10) month = `0${month}`;

    let day = _.random(1, 28);
    if (day < 10) day = `0${day}`;

    let date = `${_.random(2018, 2020)}-${month}-${day}T00:00:00Z`;

    return {
      name: `${user.first_name} ${user.last_name}`,
      amount: _.random(50, 500),
      date_joined: date
    };
  })

  let result = {
    customers,
    revenue: {
      total: 123,
      items: [
        {date: '2020-01-30T00:00:00Z', amount: 123},
        {date: '2020-02-30T00:00:00Z', amount: 123},
        {date: '2020-03-30T00:00:00Z', amount: 123},
        {date: '2020-04-30T00:00:00Z', amount: 123},
      ],
    },
  };
  return helper.sendData(result, res);
}

async function getAccountDetails(req, res) {
  try {
    let data = await helper.loadSchema(req.params, {
      userId: Joi.string().required(),
    });
  
    let accountPath = pathHelper.getDataRelative('stubs/account.json');
  
    let result = await fs.readJson(accountPath);
  
    return helper.sendData(result, res);
  } catch (err) {
    return helper.sendFailureMessage(err, res);
  }
}

async function getPlans(req, res) {
  try {
    let plansPath = pathHelper.getDataRelative('stubs/plans.json');
  
    let result = await fs.readJson(plansPath);
  
    return helper.sendData(result, res);
  } catch (err) {
    return helper.sendFailureMessage(err, res);
  }
}

async function sendMessage(req, res) {
  try {
    let data = await helper.loadSchema(req.body, {
      userId: Joi.string().required(),
      Name: Joi.string().required(),
      Email: Joi.string().email().required(),
      Honeypot: Joi.string().required(),
      Subject: Joi.string().required(),
      Message: Joi.string().required()
    });
  
    return helper.sendData({}, res);
  } catch (err) {
    return helper.sendFailureMessage(err, res);
  }
}
