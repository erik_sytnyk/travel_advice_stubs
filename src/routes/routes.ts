import homeController from '../controllers/homeController';
import apiController from '../controllers/apiController';
import authController from '../controllers/authController';
import helper from './routeHelper';

export default {
  init: initRoutes,
};

function initRoutes(app) {
  helper.init(app);

  initApiRoutes(helper);

  initAuthRoutes(helper);

  //all other routes are rendered as home (for client side routing)
  helper.get('*', homeController.home, {auth: false});
}

function initApiRoutes(helper) {
  helper.get('/me', apiController.currentUser);

  helper.get('/users', apiController.usersList);
  helper.get('/v1/users/summary', apiController.getAdminData);
  helper.get('/v1/users/:userId/account', apiController.getAccountDetails);
  
  helper.get('/search/:search', apiController.travelAdviceSearch);
  helper.get('/search/stats/:search', apiController.getStatistics);

  helper.get('/plans', apiController.getPlans);
  helper.post('/contact', apiController.sendMessage);
}

function initAuthRoutes(helper) {
  helper.post('/signup', authController.signUpPost, {auth: false});
  helper.post('/login', authController.loginPost, {auth: false});
  helper.post('/api/password-forgot', authController.forgotPassword, {auth: false});
  helper.get('/api/password-reset/:token', authController.resetPassword, {auth: false});
  helper.post('/api/password-reset', authController.resetPasswordPost, {auth: false});
}
